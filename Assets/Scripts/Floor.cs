using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{
    public PlayerController playerController;

    public GameObject target;
    public GameObject player;

    public bool isRotating;
    public bool isOnPosition;

    float width;

    // Start is called before the first frame update
    void Start()
    {
        width = target.GetComponent<MeshFilter>().mesh.bounds.extents.x;

        Debug.Log(width);
    }

    // Update is called once per frame
    void Update()
    {
        if (isRotating)
        {
            float size = player.transform.position.x - (width + target.transform.position.x);
            // Debug.Log(size);
            if (!isOnPosition)
            {
                if (size > -player.transform.position.x && size < 0 && transform.position.x < player.transform.position.x)
                {
                    // Spin the object around the target at 20 degrees/second.
                    transform.RotateAround(target.transform.position, Vector3.forward, 100 * Time.deltaTime);
                }
                else if (size < player.transform.position.x && size >= 0 && transform.localEulerAngles.z < 180)
                {
                    if (transform.localEulerAngles.z > 180)
                    {
                        Debug.Log("Here 1");
                        transform.Rotate(0, 0, 180);
                    }

                    // Spin the object around the target at 20 degrees/second.
                    transform.RotateAround(target.transform.position, Vector3.forward, 100 * Time.deltaTime);
                }
                else if (transform.position.x < player.transform.position.x)
                {
                    if (transform.localEulerAngles.z > 180)
                    {
                        Debug.Log("Here 2");
                        transform.Rotate(0, 0, 180);
                    }

                    // Spin the object around the target at 20 degrees/second.
                    transform.RotateAround(target.transform.position, Vector3.forward, 100 * Time.deltaTime);

                }
                else if (transform.position.x > player.transform.position.x && transform.localEulerAngles.z != 180)
                {
                    Debug.Log("here 3");
                    // transform.RotateAround(target.transform.position, Vector3.back, 20 * Time.deltaTime);

                    if (transform.localEulerAngles.z > 179f && transform.localEulerAngles.z < 181f)
                    {
                        // isOnPosition = true;
                    }
                }
                // else if (size < 2 && transform.position.x != player.transform.position.x)
                // {
                //     // Spin the object around the target at 20 degrees/second.
                //     transform.RotateAround(target.transform.position, Vector3.back, 100 * Time.deltaTime);
                // }

                // if (playerController.isMoving)
                // {
                //     if (player.transform.position.x - transform.position.x >= 2f + width)
                //     {
                //         // Spin the object around the target at 20 degrees/second.
                //         transform.RotateAround(target.transform.position, Vector3.forward, 1000 * Time.deltaTime);
                //     }
                // }
            }
        }
    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        Debug.Log("Enter:  " + collisionInfo.transform.tag);
        isRotating = false;
    }

    void OnCollisionExit(Collision collisionInfo)
    {
        Debug.Log("Exit: " + collisionInfo.transform.tag);
        isRotating = true;
        isOnPosition = false;
    }
}
