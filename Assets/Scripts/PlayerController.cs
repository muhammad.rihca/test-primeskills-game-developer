using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public Toggle isAcceleration;

    Animator animator;
    Rigidbody rb;

    // CharacterController characterController;
    Vector3 move;

    public bool isMoving;
    public float moveAcc;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        // characterController = GetComponent<CharacterController>();
        animator = GetComponentInChildren<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isAcceleration.isOn)
        {
            if (Input.GetKey(KeyCode.RightArrow))
            {
                transform.eulerAngles = new Vector3(0, 90, 0);
                isMoving = true;
                if (!animator.GetBool("B_Run"))
                {
                    animator.SetBool("B_Run", true);
                }
                animator.SetFloat("run_multi", 1);
                move = new Vector3(0, 0, Input.GetAxis("Horizontal Arrow"));
                Debug.Log("Update");
                transform.Translate(move * speed * Time.deltaTime);
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                transform.eulerAngles = new Vector3(0, 270, 0);
                isMoving = true;
                if (!animator.GetBool("B_Run"))
                {
                    animator.SetBool("B_Run", true);
                }
                animator.SetFloat("run_multi", 1);
                move = new Vector3(0, 0, Input.GetAxis("Horizontal Arrow"));
                Debug.Log("Update");
                transform.Translate(move * -speed * Time.deltaTime);
            }
            else
            {
                isMoving = false;
                animator.SetBool("B_Run", false);
            }
        }
    }

    void FixedUpdate()
    {
        if (isAcceleration.isOn)
        {
            if (Input.GetKey(KeyCode.RightArrow))
            {
                transform.eulerAngles = new Vector3(0, 90, 0);
                isMoving = true;
                if (!animator.GetBool("B_Run"))
                {
                    animator.SetBool("B_Run", true);
                }
                if (animator.GetFloat("run_multi") < 1)
                {
                    animator.SetFloat("run_multi", animator.GetFloat("run_multi") + 0.02f);
                }

                Vector3 velocityVector = rb.velocity;
                velocityVector.x = Mathf.Clamp(velocityVector.x + moveAcc * Time.deltaTime, 0, speed + moveAcc);

                rb.velocity = velocityVector;
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                transform.eulerAngles = new Vector3(0, 270, 0);
                isMoving = true;
                if (!animator.GetBool("B_Run"))
                {
                    animator.SetBool("B_Run", true);
                }
                if (animator.GetFloat("run_multi") < 1)
                {
                    animator.SetFloat("run_multi", animator.GetFloat("run_multi") + 0.02f);
                }

                Vector3 velocityVector = rb.velocity;
                velocityVector.x = Mathf.Clamp(velocityVector.x + moveAcc * Time.deltaTime, 0, speed + moveAcc);

                rb.velocity = velocityVector * -10;
            }
            else
            {
                if (animator.GetFloat("run_multi") > 0)
                {
                    animator.SetFloat("run_multi", animator.GetFloat("run_multi") - 0.02f);
                }
                else
                {
                    isMoving = false;
                    animator.SetBool("B_Run", false);
                }
            }
        }
    }
}
